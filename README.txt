This adds support for a lot of mods for LunaMP! (KSP Multiplayer). Plugins were created based on https://github.com/LunaMultiplayer/LunaMultiplayer/wiki/Parts-syncronization

INSTALL:

1.) Copy the folders into ..\Kerbal Space Program\GameData\LunaMultiplayer\PartSync
2.) (Optional) Install an improved version of LunaMultiplayer (https://github.com/pilotdave/LunaMultiplayer)